#Como rodar
./gradlew bootRun

#Como acessar
https://localhost:8080/conta/{ID}

Exemplos:

https://localhost:8080/conta/1  (pre-alimentado)

https://localhost:8080/conta/10
# Source Files
##grails-app/controllers/oauthfacebook/ContaController
Implementa os Endpoints conta/id e conta/cadastro/id
##grails-app/controllers/oauthfacebook/SecurityfilterInterceptor
Implementa o filtro que verifica se o usuario existe ao acessar o endpoint conta/id
##grails-app/controllers/oauthfacebook/UrlMappings
Mappeia as requisições para o controller/action apropriados
##grails-app/domain/oauthfacebook/Conta
Repositorio de dados Conta
##build.gradle
Equivalente ao pom.xml
##init/oauthfacebook/Bootstrap
Inicializa o repositorio com um registro conta/id para teste

#Observação
O código na verifica se o usuario já existe, caso seja chamado diretamente /conta/cadastro/