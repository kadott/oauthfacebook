package oauthfacebook


import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.validation.ValidationException
import groovy.json.JsonSlurper
import org.grails.web.json.JSONElement

import static org.springframework.http.HttpStatus.*

class ContaController {

    ContaService contaService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", cadastro: "GET"]


    def show(Long id) {
        respond contaService.get(id)
    }

    def cadastro(Long id, String code, String state) {
        def appId = '564694084062874'
        def appSecret = '9e1c46ca4aa1b88fea941fc073a7f7ee'
        def appToken = '564694084062874|K2Ob6yPS9s8pVYhrqZ8Z8P5hPpk'

        // requisita o access token
        RestBuilder rest = new RestBuilder()
        String url = "https://graph.facebook.com/v3.3/oauth/access_token?client_id={client_id}&redirect_uri={redirect_uri}&state={state}&client_secret={client_secret}&code={code}"
        Map params = [client_id    : appId,
                      redirect_uri : "https://localhost:8080/conta/cadastro/" + id,
                      state        : "teste",
                      client_secret: appSecret,
                      code         : code
        ]
        RestResponse restResponse = rest.get(url) {
            urlVariables params
        }

        //Extrai o access token da resposta
        JsonSlurper slurper = new JsonSlurper()
        def result1 = slurper.parseText(restResponse.json.toString())
        def accessToken = result1.access_token

        //requisita o id do usuario no facebook
        String url2 = "https://graph.facebook.com/debug_token?input_token={input_token}&access_token={access_token}"
        Map params2 = [input_token : accessToken,
                       access_token: appToken
        ]

        RestResponse restResponse2 = rest.get(url2) {
            urlVariables params2
        }

        //extrai o id do usuario no facebook da resposta
        def result2 = slurper.parseText(restResponse2.json.toString())
        def userIde = result2.data.user_id

        //requisita o nome o usuario
        String url3 = "https://graph.facebook.com/{id_usuario}/?access_token={input_token}"
        Map params3 = [id_usuario : userIde,
                       input_token: accessToken
        ]

        RestResponse restResponse3 = rest.get(url3) {
            urlVariables params3
        }
        // extrai o nome o usuario da resposta
        def result3 = slurper.parseText(restResponse3.json.toString())
        def nome = result3.name

        //persiste os dados
        def conta = new Conta(id: id, nome: nome, idFacebook: code)
        conta.id = id
        conta.save(flush: true, failOnError: true)
        //redireciona para o endpoint /conta/id
        redirect(url: "https://localhost:8080/conta/" + id)
    }

}
