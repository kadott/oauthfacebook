package oauthfacebook

class UrlMappings {

    static mappings = {
//        get "/conta/cadastro/$id?code=$code&state=$state"(controller: "conta", action: "cadastro")
        get "/conta/cadastro/$id?"(controller: "conta", action: "cadastro")
//        get "/conta/cadastro"(controller: "contaController", action: "cadastro")

        get "/$controller/$id(.$format)?"(action:"show")


        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
