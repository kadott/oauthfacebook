package oauthfacebook


class SecurityFilterInterceptor {

    /****
     * contrustor aplica o filtro a todos os endpoints
     */
    SecurityFilterInterceptor(){
        matchAll()
    }
    boolean before() {

        // se o endpoint for /conta/id
        if(controllerName == 'conta' && actionName == 'show' )
        {

            def usuario = Conta.get(params['id'])
            //se o id do usuario ja existe, mostra, senão redireciona para o login do facebook
            if (usuario) {
                return true
            } else {
                redirect(url: "https://www.facebook.com/v3.3/dialog/oauth?client_id=564694084062874&redirect_uri=https://localhost:8080/conta/cadastro/" + params['id'] + "&state=" + "teste")
            }
        }
            //se o endpoint for /conta/cadastro/id libera o acesso
            //não é verificado se o usuario ja existe nesse caso, não foi tratado, pois nao foi pedido no trabalho.
        else if (controllerName == 'conta' && actionName == 'cadastro')
        {

            return true
        }
        return false
    }

    boolean after() { false }

    void afterView() {
        // no-op
    }
}
//https://localhost:8080/cadastro/2?code=AQDA9PDpuxZemvHIMDZLtxC2pjo2e6geKlCODVgtx-mrfs9pldnUKM3Rw7tgfa6sCZNYy5_v-P07rV5O-zX50JJO0ThJXr4cgR1SrhWnE1wfIffZPzKaVVmyOXvbFjiG86CRItCTUaCTA-4JB-qQN97N_Gn-REfOrpsAhaOg5ch1eVNMd9xRe6I0hSRfKuF-HjybnkolcCJlBmhk6OsEadmvtxVLIntn-G1EnSwIy5BWfzpGWsuIwz1-Q0LDPbtyDjCsDFWkVGBIs4TJOZ91M0hZxPjfCNPIbn6jGVj080phxOO0BYjCFQbF3n4JwgJkZTb8WdbPunekacnZ1h-_3exh&state=teste#_=_
