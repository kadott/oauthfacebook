package oauthfacebook

class Conta {
    static mapping = {
        id column: 'id', generator: 'assigned'
    }
    String id
    String nome
    String idFacebook
    static constraints = {
        idFacebook(size: 0..500)

    }
}
