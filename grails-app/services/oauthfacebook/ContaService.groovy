package oauthfacebook

import grails.gorm.services.Service

@Service(Conta)
interface ContaService {

    Conta get(Serializable id)

    List<Conta> list(Map args)

    Long count()

    void delete(Serializable id)

    Conta save(Conta conta)

}